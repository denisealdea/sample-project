package com.tora;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AppTest {
    BigDecimalFunctions runner = new BigDecimalFunctions();
    BigDecimalSerialize ser = new BigDecimalSerialize();

    @Test
    public void testAdd() {
        List<BigDecimal> numbers = new LinkedList<>();
        numbers.add(BigDecimal.valueOf(4323.43029874230));
        numbers.add(BigDecimal.valueOf(9857389.893217689));
        numbers.add(BigDecimal.valueOf(74982.89362121986312));
        numbers.add(BigDecimal.valueOf(2532.5474552332));
        numbers.add(BigDecimal.valueOf(5543.9847259834653));
        BigDecimal result = new BigDecimal("9944772.749318866825");
        assertEquals(runner.Add(numbers), result);

        List<BigDecimal> numbers1 = new LinkedList<>();
        numbers1.add(BigDecimal.valueOf(4323.43029874230));
        numbers1.add(BigDecimal.valueOf(-4323.43029874230));
        numbers1.add(BigDecimal.valueOf(74982.89362121986312));
        numbers1.add(BigDecimal.valueOf(-74982.89362121986312));
        numbers1.add(BigDecimal.valueOf(5543.9847259834653));
        assertEquals(runner.Add(numbers1), BigDecimal.valueOf(5543.9847259834653));
    }

    @Test
    public void testAverage() {
        List<BigDecimal> numbers = new LinkedList<>();
        numbers.add(BigDecimal.valueOf(4323.43029874230));
        numbers.add(BigDecimal.valueOf(9857389.893217689));
        numbers.add(BigDecimal.valueOf(74982.89362121986312));
        numbers.add(BigDecimal.valueOf(2532.5474552332));
        numbers.add(BigDecimal.valueOf(5543.9847259834653));
        BigDecimal result = new BigDecimal("1988954.549863773365");
        assertEquals(runner.Average(numbers), result);
    }

    @Test
    public void testTop() {
        List<BigDecimal> numbers = new LinkedList<>();
        numbers.add(BigDecimal.valueOf(4323.43029874230));
        numbers.add(BigDecimal.valueOf(9857389.893217689));
        numbers.add(BigDecimal.valueOf(74982.89362121986312));
        numbers.add(BigDecimal.valueOf(2532.5474552332));
        numbers.add(BigDecimal.valueOf(5543.9847259834653));
        List<BigDecimal> result = new LinkedList<>();
        result.add(BigDecimal.valueOf(9857389.893217689));
        assertEquals(runner.top10PercentNumbers(numbers), result);
    }

    @Test
    public void testSerialize() {
        Map<String, byte[]> map = ser.serialize(BigDecimal.valueOf(2445.23453312));
        String result = ser.deserialize(map);
        assertEquals(result, "2445.23453312");
    }
}
