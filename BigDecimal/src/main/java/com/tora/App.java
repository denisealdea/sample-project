package com.tora;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.*;

import static java.lang.Integer.parseInt;

public class App {
    public static void main(String[] args) {
        BigDecimalSerialize ser = new BigDecimalSerialize();
        Map<String, byte[]> map = ser.serialize(BigDecimal.valueOf(2445.23453312));
        System.out.println(map);
        System.out.println(ser.deserialize(map));

        BigDecimalFunctions runner = new BigDecimalFunctions();
        String lines;
        Scanner reader = new Scanner(System.in);
        System.out.print(">> ");
        lines = reader.nextLine();
        while (!Objects.equals(lines, "stop")) {
            String[] parts = lines.split(" ");
            List<BigDecimal> numbers = new LinkedList<>();
            BigDecimal nr;
            for (String elem : parts) {
                nr = new BigDecimal((elem));
                numbers.add(nr);
            }
            System.out.println(" > 1. Add");
            System.out.println(" > 2. Average");
            System.out.println(" > 3. Top 10% numbers");
            System.out.print(" > ");
            lines = reader.nextLine();
            while (!Objects.equals(lines, "exit")) {
                if (Objects.equals(lines, "1"))
                    System.out.println("  >>> " + runner.Add(numbers));
                else if (Objects.equals(lines, "2"))
                    System.out.println("  >>> " + runner.Average(numbers));
                else if (Objects.equals(lines, "3"))
                    System.out.println("  >>> " + runner.top10PercentNumbers(numbers));
                else
                    System.out.println("    >>> Command not found");
                System.out.print(" > ");
                lines = reader.nextLine();
            }
            System.out.print(">> ");
            lines = reader.nextLine();
        }
    }
}
