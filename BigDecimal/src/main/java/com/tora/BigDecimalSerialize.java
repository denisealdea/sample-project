package com.tora;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class BigDecimalSerialize {
    public Map<String, byte[]> serialize(BigDecimal d) {
        String s = d.toString();
        String[] split = s.split("\\.");
        byte[] data1 = Base64.getDecoder().decode(split[0]);
        byte[] data2 = Base64.getDecoder().decode(split[1]);
        Map<String, byte[]> result = new HashMap<>();
        result.put("firstValue", data1);
        result.put("secondValue", data2);
        return result;
    }
    public String deserialize(Map<String, byte[]> d) {
        String data1 = Base64.getEncoder().encodeToString(d.get("firstValue"));
        String data2 = Base64.getEncoder().encodeToString(d.get("secondValue"));
        return data1 + "." + data2;
    }
}
