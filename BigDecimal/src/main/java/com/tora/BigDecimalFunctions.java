package com.tora;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalFunctions {
    public BigDecimal Add(List<BigDecimal> numbers) {
        return numbers.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal Average(List<BigDecimal> numbers) {
        BigDecimal sum = numbers.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
        return sum.divide(BigDecimal.valueOf(numbers.size()));
    }

    public List<BigDecimal> top10PercentNumbers(List<BigDecimal> numbers) {
        long limit = (long) Math.ceil(10 * numbers.size() / 100.0);
        return numbers.stream().sorted((n1, n2) -> n2.compareTo(n1)).limit(limit).collect(Collectors.toList());
    }
}
