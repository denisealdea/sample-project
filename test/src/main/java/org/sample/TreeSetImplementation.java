package org.sample;

import java.util.TreeSet;

public class TreeSetImplementation<T> {
    private TreeSet<T> ts;

    public TreeSetImplementation() {
        ts = new TreeSet<T>();
    }

    public boolean add(T elem){
        return ts.add(elem);
    }

    public boolean contains(T elem){
        return ts.contains(elem);
    }

    public boolean remove(T elem){
        return ts.remove(elem);
    }

}
