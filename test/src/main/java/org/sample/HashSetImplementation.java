package org.sample;

import java.util.HashSet;

public class HashSetImplementation<T> {
    private HashSet<T> elements;

    public HashSetImplementation() {
        elements = new HashSet<>();
    }

    public boolean add(T e) {
        return elements.add(e);
    }

    public boolean contains(T e) {
        return elements.contains(e);
    }

    public boolean delete(T e) {
        return elements.remove(e);
    }
}
