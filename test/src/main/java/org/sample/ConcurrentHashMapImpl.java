package org.sample;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapImpl<K, V> {
    ConcurrentHashMap<K,V> chm;

    public ConcurrentHashMapImpl(){
        chm = new ConcurrentHashMap<>();
    }

    public V add(K key, V value){
        return chm.put(key, value);
    }

    public boolean contains(V value){
        return chm.contains(value);
    }

    public boolean remove(K key, V value){
        return chm.remove(key, value);
    }

}
