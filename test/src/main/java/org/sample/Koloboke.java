package org.sample;

import com.koloboke.collect.map.hash.HashIntIntMap;
import com.koloboke.collect.map.hash.HashIntIntMaps;

public class Koloboke {
    HashIntIntMap elements;

    public Koloboke() {
        elements = HashIntIntMaps.newMutableMap();
    }

    public int add(int nr1, int nr2) {
        return elements.put(nr1, nr2);
    }

    public boolean contains(int nr1) {
        return elements.containsKey(nr1);
    }

    public boolean remove(int nr1, int nr2) {
        return elements.remove(nr1, nr2);
    }
}
