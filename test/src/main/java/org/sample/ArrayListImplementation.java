package org.sample;

import java.util.ArrayList;

public class ArrayListImplementation<T> {
    private ArrayList<T> elements;

    public ArrayListImplementation() {
        elements = new ArrayList<>();
    }

    public boolean add(T e) {
        return elements.add(e);
    }

    public boolean remove(int i) {
        if (i < 0 || i >= elements.size()) {
            return false;
        }
        elements.remove(elements.get(i));
        return true;
    }

    public boolean contains(T e) {
        return elements.contains(e);
    }
}
