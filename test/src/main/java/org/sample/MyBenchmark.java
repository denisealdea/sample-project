/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.sample;

import org.openjdk.jmh.annotations.Benchmark;

public class MyBenchmark {

    @Benchmark
    public void testHashSet() {
        HashSetImplementation<Object> elements = new HashSetImplementation<>();
        elements.add(1);
        elements.add(3);
        assert (elements.contains(1));
        elements.delete(3);
    }

    @Benchmark
    public void testArrayList() {
        ArrayListImplementation<Object> elements = new ArrayListImplementation<>();
        elements.add(1);
        elements.add(3);
        assert (elements.contains(1));
        elements.remove(3);
    }

    @Benchmark
    public void testConcurrentHash() {
        ConcurrentHashMapImpl<Object, Object> elements = new ConcurrentHashMapImpl<>();
        elements.add(1, 1);
        elements.add(3, 1);
        assert (elements.contains(1));
        elements.remove(3, 1);
    }

    @Benchmark
    public void testEclipse() {
        EclipseCollection<Object> elements = new EclipseCollection<>();
        elements.add(1);
        elements.add(3);
        elements.remove(3);
        assert (elements.contains(1));
        assert (!elements.contains(3));
    }

    @Benchmark
    public void testKoloboke() {
        Koloboke elements = new Koloboke();
        elements.add(1, 1);
        elements.add(3, 3);
        assert (elements.contains(1));
        elements.remove(3, 3);
    }

//    @Benchmark
//    public void testTreeSet() {
//        TreeSetImplementation<Object> elements = new TreeSetImplementation<>();
//        elements.add(1);
//        elements.add(3);
//        elements.remove(3);
//        assert (elements.contains(1));
//        assert (!elements.contains(3));
//    }
}
