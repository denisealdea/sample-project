package com.tora;

public enum Protocol {
    INIT("!hello "), ACK("!ack"), DONE_CHAT("!bye");

    private final String header;

    Protocol(final String header) {
        this.header = header;
    }

    public String header() {
        return header;
    }
}
