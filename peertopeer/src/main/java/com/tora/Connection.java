package com.tora;

import java.io.*;
import java.net.Socket;

public final class Connection {

    private final Socket socket;
    private final BufferedReader reader;
    private final BufferedWriter writer;

    public Connection(final Socket socket) throws IOException {
        this.socket = socket;
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    public BufferedReader reader() {
        return reader;
    }

    public BufferedWriter writer() {
        return writer;
    }

    public void close() throws IOException {
        reader.close();
        writer.close();
        socket.close();
    }

    @Override
    public String toString() {
        return "Host: " + socket.getInetAddress().getHostAddress() + "\n" +
                "Port: " + socket.getPort();
    }
}
