package com.tora.ui.chat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Chat {

    private final BufferedReader reader;
    private final BufferedWriter writer;
    private final Scanner scanner;

    private final ExecutorService executor = Executors.newFixedThreadPool(2);

    public Chat(final BufferedReader reader, final BufferedWriter writer, final Scanner scanner) {
        this.reader = reader;
        this.writer = writer;
        this.scanner = scanner;
    }

    public void start() throws InterruptedException {
        var condition = new SynchronizationCondition(false);

        executor.submit(new Reader(reader, condition));
        executor.submit(new Writer(scanner, writer, condition));

        while (!condition.done()) {
            condition.await();
        }
    }

    public void close() {
        System.out.println("Chat closed");
        executor.shutdown();
    }
}
