package com.tora.ui;

import com.tora.Client;
import com.tora.Connection;
import com.tora.Protocol;
import com.tora.ui.chat.Chat;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class CommandLineInterface {

    private boolean done = false;
    private final Scanner scanner = new Scanner(System.in);
    private final Client client;

    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    public CommandLineInterface(final Client client) {
        this.client = client;
    }

    public void run() throws IOException, InterruptedException {
        println("Welcome in the chat, to show the commands write !commands:");
        while (!done) {
            final String command = scanner.nextLine().trim();
            handleCommand(command);
        }
    }

    private void printMenu() {
        println(
                """
                        !commands (this will show the current available commands)
                        !hello <ip> <host> (this will create a connection with another person)
                        !ack (this is accept the connection)
                        !bye (this will end the chat with the person that you are currently talking)
                        !byebye (this will end your entire session)
                        """
        );
    }

    private void handleCommand(final String command) throws IOException, InterruptedException {
        String[] commands = command.split(" ");
        switch (commands[0]) {
            case "!commands" -> printMenu();
            case "!hello" -> addFriend(commands[1], Integer.parseInt(commands[2]));
            case "!ack" -> chat();
            case "!bye" -> disconnectFromFriend();
            case "!byebye" -> exit();
            default -> println("Try again");
        }
    }


    private void chat() throws InterruptedException {
        final Connection chatFriend = client.connections().get(client.connections().size() - 1);
        handleChat(chatFriend);
    }

    private void handleChat(final Connection friend) throws InterruptedException {
        printChatInstructions();

        final Chat chat = new Chat(friend.reader(), friend.writer(), scanner);
        chat.start();
        chat.close();
    }

    private void printChatInstructions() {
        println("Write '" + Protocol.DONE_CHAT.header() + "' to quit the chat");
    }


    private static final class FriendRequest implements Runnable {
        private final Client sender;
        private final String host;
        private final int port;

        public FriendRequest(final Client sender, final String host, final int port) {
            this.sender = sender;
            this.host = host;
            this.port = port;
        }

        @Override
        public void run() {
            try {
                final String status = sender.connectToFriend(host, port);
                println(status);
            } catch (IOException | ExecutionException | InterruptedException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void addFriend(String host, int port) {
        executor.submit(new FriendRequest(client, host, port));
    }

    private void disconnectFromFriend() throws IOException {
        client.disconnectFromFriend(client.connections().size() - 1);
    }

    private void exit() throws IOException {
        shutdownInterface();
        shutdownScanner();
        shutdownClient();
        shutdownChatExecutor();
        System.out.println("Goodbye!");
    }

    private void shutdownInterface() {
        done = true;
    }

    private void shutdownScanner() {
        scanner.close();
    }

    private void shutdownClient() throws IOException {
        client.close();
    }

    private void shutdownChatExecutor() {
        executor.shutdownNow();
    }


    private static void println(final Object obj) {
        System.out.println(obj);
    }
}
