package com.tora;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;

public final class Client {

    private final String host;
    private final int port;

    private final List<Connection> connections = new ArrayList<>();

    private final ExecutorService connectionExecutor = Executors.newFixedThreadPool(5);
    private final ServerSocket server;

    public Client(final String host, final int port) throws IOException {
        this.host = host;
        this.port = port;
        this.server = new ServerSocket(port);
        listenForFriends();
    }

    public List<Connection> connections() {
        return connections;
    }


    public String connectToFriend(final String host, final int port) throws IOException, ExecutionException, InterruptedException {
        final var socket = new Socket(host, port);
        final var request = new ConnectionRequest(socket);

        final Future<Optional<Connection>> optionalFutureFriend = connectionExecutor.submit(request);
        final Optional<Connection> optionalFriend = optionalFutureFriend.get();

        if (optionalFriend.isEmpty()) {
            return "Connection refused";
        } else {
            connections.add(optionalFriend.get());
            return "Connection accepted";
        }
    }


    private static final class ConnectionRequest implements Callable<Optional<Connection>> {

        private final Socket socket;

        private ConnectionRequest(final Socket socket) {
            this.socket = socket;
        }

        @Override
        public Optional<Connection> call() throws Exception {
            final String response = initiateConnection();
            return decideConnection(response);
        }

        private String initiateConnection() throws IOException {
            sendInit();
            return receiveResponse();
        }

        private void sendInit() throws IOException {
            final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            writer.write(Protocol.INIT.header() + "\n");
            writer.flush();
        }

        private String receiveResponse() throws IOException {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            return reader.readLine();
        }

        private Optional<Connection> decideConnection(final String response) throws IOException {
            if (Protocol.ACK.header().equals(response)) {
                final Connection connection = new Connection(socket);
                return Optional.of(connection);
            } else {
                return Optional.empty();
            }
        }
    }


    public void listenForFriends() {
        connectionExecutor.submit(() -> {
            while (true) {
                final Socket friend = server.accept();
                handleFriendConnection(friend);
            }
        });
    }

    private void handleFriendConnection(final Socket friend) throws IOException {
        final Connection friendConnection = new Connection(friend);
        addFriend(friendConnection);
        acknowledgeFriend(friendConnection);
    }

    private void addFriend(final Connection friendConnection) {
        connections.add(friendConnection);
    }

    private static void acknowledgeFriend(final Connection friendConnection) throws IOException {
        friendConnection.writer().write(Protocol.ACK.header() + "\n");
        friendConnection.writer().flush();
    }


    public void disconnectFromFriend(int index) throws IOException {
        final Connection connection = connections.get(index);
        connection.close();
    }


    public void close() throws IOException {
        shutdownConnectionExecutor();
        shutdownServer();
        closeConnections();
    }

    private void shutdownConnectionExecutor() {
        connectionExecutor.shutdown();
    }

    private void closeConnections() throws IOException {
        for (final Connection connection : connections) {
            connection.close();
        }
    }

    private void shutdownServer() throws IOException {
        server.close();
    }
}
