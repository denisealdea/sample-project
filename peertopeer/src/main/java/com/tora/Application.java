package com.tora;

import com.tora.ui.CommandLineInterface;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException, InterruptedException {
        final Client client = new Client("127.0.0.1", Integer.parseInt(args[0]));
        final CommandLineInterface cmd = new CommandLineInterface(client);
        cmd.run();
    }
}
