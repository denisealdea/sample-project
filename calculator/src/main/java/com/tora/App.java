package com.tora;

import java.util.Objects;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;
        while (true) {
            input = scanner.nextLine();
            String command[] = input.split(" ");
            if (Objects.equals(command[0], "sqrt")) {
                Operation operation = new Operation(Operator.sqrt, Double.parseDouble(command[1]));
                System.out.println(operation.executeOperation());
            } else if (Objects.equals(command[0], "max")) {
                Operation operation = new Operation(Operator.max, Double.parseDouble(command[1]), Double.parseDouble(command[2]));
                System.out.println(operation.executeOperation());
            } else if (Objects.equals(command[0], "min")) {
                Operation operation = new Operation(Operator.min, Double.parseDouble(command[1]), Double.parseDouble(command[2]));
                System.out.println(operation.executeOperation());
            } else if (Objects.equals(command[0], "exit")) {
                break;
            } else if (Objects.equals(command[1], "+")) {
                Operation operation = new Operation(Operator.add, Double.parseDouble(command[0]), Double.parseDouble(command[2]));
                System.out.println(operation.executeOperation());
            } else if (Objects.equals(command[1], "-")) {
                Operation operation = new Operation(Operator.subtract, Double.parseDouble(command[0]), Double.parseDouble(command[2]));
                System.out.println(operation.executeOperation());
            } else if (Objects.equals(command[1], "*")) {
                Operation operation = new Operation(Operator.multiply, Double.parseDouble(command[0]), Double.parseDouble(command[2]));
                System.out.println(operation.executeOperation());
            } else if (Objects.equals(command[1], "/")) {
                Operation operation = new Operation(Operator.divide, Double.parseDouble(command[0]), Double.parseDouble(command[2]));
                System.out.println(operation.executeOperation());
            }
        }
    }
}
