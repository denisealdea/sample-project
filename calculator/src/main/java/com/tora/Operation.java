package com.tora;

public class Operation {
    public Operator operator;
    public double number1, number2;

    public Operation(Operator o, double n1, double n2) {
        operator = o;
        number1 = n1;
        number2 = n2;
    }

    public Operation(Operator o, double n) {
        operator = o;
        number1 = n;
    }

    public double executeOperation() {
        switch (operator) {
            case add:
                return number1 + number2;
            case subtract:
                return number1 - number2;
            case multiply:
                return number1 * number2;
            case divide:
                return number1 / number2;
            case max:
                return Math.max(number1, number2);
            case min:
                return Math.min(number1, number2);
            case sqrt:
                return Math.sqrt(number1);
        }
        return 0;
    }
}
