package com.tora;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void testOperations()
    {
        Operation op1 = new Operation(Operator.add, 10, 22);
        Operation op2 = new Operation(Operator.subtract, 100, 35);
        Operation op3 = new Operation(Operator.multiply, 2, 15);
        Operation op4 = new Operation(Operator.divide, 35, 7);
        Operation op5 = new Operation(Operator.max, 10, 22);
        Operation op6 = new Operation(Operator.min, 10, 22);
        Operation op7 = new Operation(Operator.sqrt, 100);

        assertTrue(op1.executeOperation() == 32);
        assertTrue(op2.executeOperation() == 65);
        assertTrue(op3.executeOperation() == 30);
        assertTrue(op4.executeOperation() == 5);
        assertTrue(op5.executeOperation() == 22);
        assertTrue(op6.executeOperation() == 10);
        assertTrue(op7.executeOperation() == 10);
    }
}
